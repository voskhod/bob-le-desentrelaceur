#/bin/sh

if [[ -f "$1" ]]; then 
  remove_uselless_lines="/SLICE/d;"
  remove_uselless_lines+="/.*SEQUENCE_REPEATED.*/d;"
  remove_uselless_lines+="/.*SEQUENCE_MODIFIED.*/d;"
  remove_uselless_lines+="/.*CLOSED.*/d;"
  remove_uselless_lines+="/libmpeg2-0.5.1 -.*and Aaron Holtzman/d;"
  remove_uselless_lines+="/.*GOP.*/d;"
  remove_uselless_lines+="/.*END.*/d;"
  remove_uselless_lines+="/.*bad sync byte.*/d;"

  extract_flags="s/.*PICTURE . (.*)/\1/;"
  extract_flags+="s/(.*) time_ref .*/\1/;"
  extract_flags+="s/(.*)fields 2(.*)/\1\2/;"

  extract_dim_and_fps="s/.*( [0-9]+x[0-9]+ chroma [0-9]+x[0-9]+ fps [0-9]+\.).*/\1/;"
  extract_dim_and_fps+="s/^ (.*chroma.*)/\1/;"
  extract_dim_and_fps+="s/(.*chroma.*).$/\1/;"

  cat "$1" | sed -E -r -e "$remove_uselless_lines $extract_flags $extract_dim_and_fps" 
else
  >&2 echo 'Error: log not found'
  exit 1
fi
