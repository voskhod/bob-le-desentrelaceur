# Bob le desentrelaceur 👷

This project has done for the TVID (video processing) course at EPITA. The
objecive was to build a deinterlacer. Bob le desentrelaceur is capable of
loading, processing and rendering videos from images and flags extracted by
[mpeg2dec](https://github.com/aholtzma/mpeg2dec).

They are still some room from improvement in particular on performance: on my PC
I barely achived 30fps with the adaptive deinterlacer. Here some ideas:
* remove many uselless memory allocation and copy by doing the convertion on the
  fly
* Pool allocator ? (all the images are the same size)
* implementing better desentrelacer.

# Build

```sh
cargo build --release
```

# Usage

First we need to extract the images and flags from our video:
```sh
mpeg2dec VIDEO_PATH -o pgm -v 2> raw
```
Then we need to process the flags:
```
./process_log.sh raw > log.txt
```
And finally, we can display it using bob:
```
cargo run -- .
```
For finner tunning check `--help` page.

