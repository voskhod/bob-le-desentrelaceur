use colored::Colorize;

static mut VERBOSE: u8 = 2;

// This is unsafe as we could have some data race but we don't care.
pub fn set_verbose_level(level: u8) {
    unsafe {
        VERBOSE = level;
    }
}

#[allow(dead_code)]
pub fn err(msg: impl Into<String>) {
    println!("[{}] {}", "ERROR".red(), msg.into());
}

#[allow(dead_code)]
pub fn warn(msg: impl Into<String>) {
    unsafe {
        if cfg!(debug_assertions) || VERBOSE >= 1 {
            println!("[{}] {}", "WARN".yellow(), msg.into());
        }
    }
}

#[allow(dead_code)]
pub fn info(msg: impl Into<String>) {
    unsafe {
        if cfg!(debug_assertions) || VERBOSE >= 2 {
            println!("[{}] {}", "INFO".green(), msg.into());
        }
    }
}

#[allow(dead_code)]
pub fn debug(msg: impl Into<String>) {
    if cfg!(debug_assertions) {
        println!("[{}] {}", "DEBUG".blue(), msg.into());
    }
}
