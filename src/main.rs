mod log;
mod convert;
mod deinterlace;
mod video_flags;

use std::fs;
use std::error::Error;
use fltk::{app, draw, enums::*, frame, prelude::*, window, button::Button};
use image::io::Reader as ImageReader;
use clap::{Arg, App};
use std::{rc, cell};
use std::time::Instant;
use deinterlace::DeinterlacedFrames;
use video_flags::VideoFlags;
use std::fmt;

// Error to handle fail during loading and decoding of the images.
#[derive(Debug)]
enum LoadError {
    FileNotFound(String),
    InvalidFileFormat(String)
}

impl fmt::Display for LoadError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Error while loading !")
    }
}

impl Error for LoadError {
}

// Load the ith image and deinterlace if needed. `displacement_threshold` and
// flags are used by the deinterlacer.
fn load_and_process(i: usize,
                    directory: &str,
                    displacement_threshold: i32,
                    flags: &VideoFlags)
                                    -> Result<DeinterlacedFrames, LoadError> {

    let file = format!("{}/{}.pgm", directory, i);

    let im = ImageReader::open(&file);
    if im.is_err() {
        return Err(LoadError::FileNotFound(file));
    }

    let im = im.unwrap().decode();
    if im.is_err() {
        return Err(LoadError::InvalidFileFormat(file));
    }

    let im = im.unwrap().to_luma8();
    Ok(DeinterlacedFrames::new(&im, i, flags, displacement_threshold))
}

// Load, process and display in realtime the video. If playloop is activated,
// the video is played in loop. This function can only fail at the
// initialization, if something bad happen after the frame responsible will be
// ignored.
fn render_loop(fps: u8,
                directory: &str,
                playloop: bool,
                displacement_threshold: i32,
                flags: &VideoFlags)
                                                -> Result<(), Box<dyn Error>> {

    // Create the window with the same dimensions of the first image.
    let (w, h) = flags.dimensions();
    let app = app::App::default();
    let mut wind = window::Window::default().with_size(w as i32, h as i32);
    let mut frame = frame::Frame::default().size_of(&wind);
    let mut but = Button::new(w as i32/2-40, h as i32/2-20, 80, 40, "@+6>");

    wind.end();
    wind.show();

    // The index of image to show.
    let i = rc::Rc::from(cell::RefCell::from(0));
    // The videos is paused or not ?
    let play = rc::Rc::from(cell::RefCell::from(false));

    frame.draw({
        let i = i.clone();
        let play = play.clone();
        let directory = directory.to_string();
        let flags = flags.clone();

        // Load the first frames. If the directory contains no images, this
        // function returns an error.
        let mut frames = load_and_process(0,
                                            &directory,
                                            displacement_threshold,
                                            &flags)?;

        // This will be called at each redisplay.
        move |f| {

            // We always display first as we want to redisplay as fast as we
            // can, then we can load the image during the time between two
            // frame.
            draw::draw_image(frames.next(),
                f.x(), f.y(),
                f.w(), f.h(),
                ColorDepth::Rgb8
            ).unwrap();

            if !frames.empty() {
                return;
            }

            // Load and process the next image. If something fails, we ignore it
            // and continue.
            let mut cur = *i.borrow();
            loop {
                match load_and_process(cur, &directory, displacement_threshold, &flags) {

                    Err(LoadError::FileNotFound(_)) => {
                        // The flux is finished. Restore the flux to its
                        // orignal state and pause the rendering if we don't
                        // want to play in loop.
                        *play.borrow_mut() ^= !playloop;
                        cur = 0;
                    }
                    Err(LoadError::InvalidFileFormat(file)) => {
                        log::warn(format!(
                                "Could not decode {}, ignore it and continue",
                                file));
                        cur += 1;
                    }
                    Ok(new_frames) => {
                        frames = new_frames;
                        break;
                    }
                }
            }
            *i.borrow_mut() = cur;
        }
    });

    // Button play/pause.
    // FIXME: There is a bug when SPACE is pressed, the button will pause and
    // resume the videos but the frame rate will increase to compensate the
    // time paused. Not sure if the bug if from here or below with wait()...
    but.set_callback({
        let play = play.clone();
        move |_| {
            *play.borrow_mut() ^= true;
        }
    });

    let time_between_frame = 1.0 / fps as f64;
    let mut prev_fps_counter = Instant::now();
    let mut prev_frame = Instant::now();
    let mut count = 0;
    let mut time_to_wait = 0.0;

    while app::wait() {

        if *play.borrow() {
            // Estimate the number of frame per seconds by computing the average
            // time for the last 4 seconds.
            if count == fps*4 {

                let elapsed = prev_fps_counter.elapsed().as_secs_f64();
                log::info(format!("{:.2} fps (average of the last {} frames)",
                    1.0 / (elapsed / (fps*4) as f64), fps*4));
                prev_fps_counter = Instant::now();
                count = 0;
            }

            count += 1;
            *i.borrow_mut() += 1;
            frame.redraw();

            // As sleep may sleep longer than the duration specified, we need
            // to add the delay. The duration from the last frame contains the
            // the time slept, the delay and the redisplay of the current frame,
            // so we remove the last time_to_wait to obtain the delay and the
            // compute time.
            let delta_t = prev_frame.elapsed().as_secs_f64() - time_to_wait;
            time_to_wait = time_between_frame - delta_t;
        } else {
            count = 0;
            prev_fps_counter = Instant::now();
            time_to_wait = time_between_frame;
        }

        prev_frame = Instant::now();
        app::sleep(time_to_wait);
    }

    Ok(app.run()?)
}


fn process_all_to_ppm(in_dir: &str,
                        out_dir: &str,
                        displacement_threshold: i32,
                        flags: &VideoFlags)
                                                -> Result<(), Box<dyn Error>> {

    // Check if the directory exist, if not create it. If the creation fail, the
    // images will be store in the directory where it was run.
    let out_dir =  {
        match fs::create_dir(out_dir) {
            Err(err) => {
                if err.kind() == std::io::ErrorKind::AlreadyExists {
                    out_dir
                } else {
                    log::err(format!("Directory does not exist and it cound \
                            not be created : {}. The images will be save in
                            \".\"", err));
                    "."
                }
            },
            Ok(_) => out_dir
        }
    };

    let mut in_idx = 0;
    let mut out_idx = 0;
    loop {
        // Same as the render loop, load and process the frames then save them.
        // If something goes wrong, we ingore the image and continue.
        match load_and_process(in_idx, in_dir, displacement_threshold, flags) {

            Err(LoadError::FileNotFound(_)) => {
                return Ok(());
            }
            Err(LoadError::InvalidFileFormat(file)) => {
                log::warn(format!(
                        "Could not decode {}, ignore it and continue",
                        file));
            }
            Ok(mut frames) => {

                frames.next().save(format!("{}/{}.ppm", out_dir, out_idx))?;
                out_idx += 1;

                if !frames.empty() {
                    frames.next().save(format!("{}/{}.ppm", out_dir, out_idx))?;
                    out_idx += 1;
                }
            }
        }
        in_idx += 1;
    }
}


fn main() -> Result<(), Box<dyn Error>> {

    let matches = App::new("Bob le désentrelaceur \u{1F477}") // Hard hat emoji
        .version("1.0")
        .author("Alexandre Bourquelot & Jérôme Dubois")
        .about("Decompress and display video flux")
        .arg(Arg::with_name("ppm")
            .long("ppm")
            .value_name("OUT_DIRECTORY")
            .takes_value(true)
            .multiple(false)
            .help("Save the images as ppm (by default \"results\")")
        )
        .arg(Arg::with_name("fps")
            .long("fps")
            .value_name("REFRESH_RATE")
            .takes_value(true)
            .multiple(false)
            .conflicts_with("ppm")
            .help("Force the refresh rate")
        )
        .arg(Arg::with_name("verbose")
            .long("verbose")
            .short("v")
            .value_name("LEVEL")
            .takes_value(true)
            .multiple(false)
            .help("Sets the level of verbosity (between 0 and 2)")
        )
        .arg(Arg::with_name("loop")
            .long("loop")
            .conflicts_with("ppm")
            .help("Play the videos in loop")
        )
        .arg(Arg::with_name("directory")
            .required(true)
            .multiple(false)
            .help("The directory containing the extracted images")
        )
        .arg(Arg::with_name("flags")
            .long("flags")
            .short("f")
            .value_name("FLAGS")
            .takes_value(true)
            .multiple(false)
            .help("The file containing the flags extracted. If not specified \
                    DIRECTORY/log.txt will be used.")
        )
        .arg(Arg::with_name("threshold")
            .long("displacement-threshold")
            .value_name("THR")
            .takes_value(true)
            .multiple(false)
            .help("Threashold for the adaptive spatial deinterlacer \
                    (default: 150)")
        )
        .get_matches();


    // This is messy :(. Get all parameter and flags or set them with their
    // default value.
    let verbose : u8 = match matches.value_of("verbose") {
        Some(val_str) => { val_str.parse()? }
        None => 0
    };
    log::set_verbose_level(verbose);

    let dir = matches.value_of("directory").unwrap();
    log::info(format!("Read images from {}", dir));

    let displacement_threshold: i32 = match matches.value_of("threshold") {
        Some(val_str) => { val_str.parse()? },
        None => 150
    };

    let flags = VideoFlags::new({
        let flags_path = match matches.value_of("flags") {
            Some(path) => path.to_string(),
            None => format!("{}/{}", dir, "log.txt")
        };
        log::info(format!("Read flags from {}", flags_path));
        flags_path
    });
    if let Err(e) = flags {
        log::err("Flags not found !");
        return Err(Box::new(e));
    }
    let flags = flags.unwrap();

    if matches.is_present("ppm") {

        let out_dir = match matches.value_of("ppm") {
            Some(dir) => { dir },
            None => { "results" }
        };

        process_all_to_ppm(dir, out_dir, displacement_threshold, &flags)
    } else {
        let fps: u8 = match matches.value_of("fps") {
            Some(val_str) => { val_str.parse()? }
            None => flags.fps()
        };

        render_loop(fps,
                    dir,
                    matches.is_present("loop"),
                    displacement_threshold,
                    &flags)
    }
}
