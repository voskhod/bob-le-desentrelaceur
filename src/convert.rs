use image::{RgbImage, Rgb, GrayImage};


pub fn yuv2rgb(img: &GrayImage, luma_dim: (u32, u32), chroma_dim: (u32, u32))
                                                                -> RgbImage {

    let (luma_w, luma_h) = luma_dim;
    let (chroma_w, chroma_h) = chroma_dim;

    let mut res = image::RgbImage::new(luma_w, luma_h);

    for j in 0..luma_h {
        for i in 0..luma_w {

            // Find the position of the pixel scale to the chroma (it is smaller
            // than the luma).
            let i_chroma = (i*chroma_w)/luma_w;
            let j_chroma = (j*chroma_h)/luma_h;

            let y = img.get_pixel(i, j)[0] as i32;
            let u = img.get_pixel(i_chroma, j_chroma+luma_h)[0] as i32;
            let v = img.get_pixel(i_chroma+chroma_w, j_chroma+luma_h)[0] as i32;

            // Fast integer convertion YUV to RGB.
            let r = y + ((91881*v) >> 16) - 179;
            let g = y - (((22544*u) + 46793*v) >> 16) + 135;
            let b = y + ((116129*u) >> 16) - 226;

            res.put_pixel(i, j, Rgb([clamp(r), clamp(g), clamp(b)]));
        }
    }

    res
}

fn clamp(i: i32) -> u8 {

    match i {
        i if i < 0 => 0,
        i if i > 255 => 255,
        i => i as u8
    }
}
