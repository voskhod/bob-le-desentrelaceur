use std::fs::File;
use std::io::BufRead;
use std::path::Path;
use std::iter::Peekable;
use std::io::Error;


#[derive(Debug, Copy, Clone)]
pub enum SamplingMode {
    S422,
}

#[derive(Debug, Clone)]
pub struct VideoFlags {

    dim: (u32, u32),
    chroma_dim: (u32, u32),
    fps: u8,
    is_progressive: Vec<bool>,
    is_top_first_frame: Vec<bool>,
    _sampling: SamplingMode
}

impl VideoFlags {

    pub fn new<P>(filename: P) -> Result<Self, Error>
    where P: AsRef<Path>
    {
        let file = File::open(filename)?;
        let lines = std::io::BufReader::new(file).lines();

        // Try to find an upper bound for the number of line wich gives us an
        // hint for the size of the vectors.
        let hint_size = {
            let (lower, higher) = lines.size_hint();
            match higher {
                None => lower,
                Some(higher) => higher
            }
        };

        let mut flags = VideoFlags {
            dim: (0, 0),
            chroma_dim: (0, 0),
            fps: 0,
            is_progressive: Vec::with_capacity(hint_size),
            is_top_first_frame: Vec::with_capacity(hint_size),
            _sampling: SamplingMode::S422
        };

        let mut is_first_line = true;

        for line in lines {

            if line.is_err() {
                break;
            }

            let line = line.unwrap();
            let mut it = line.chars().peekable();

            if is_first_line {
                // The first line is the "header" with the size of the image
                // and chroma and the number of fps.
                let width = get_number(&mut it);
                assert_eq!(it.next(), Some('x'));
                let heigth = get_number(&mut it);
                flags.dim = (width, heigth);

                assert!(is_word(&mut it, " chroma "));
                let width = get_number(&mut it);
                assert_eq!(it.next(), Some('x'));
                let heigth = get_number(&mut it);
                flags.chroma_dim = (width, heigth);

                assert!(is_word(&mut it, " fps "));
                flags.fps = get_number(&mut it) as u8;

                is_first_line = false;
            } else {

                // Each line containts the flags of the images.

                let b = is_word(&mut it, "PROG");
                flags.is_progressive.push(b);
                if b {
                    assert!(is_word(&mut it, "  "));
                }

                flags.is_top_first_frame.push(is_word(&mut it, "TFF"));
            }
        }

        Ok(flags)
    }

    pub fn dimensions(&self) -> (u32, u32) {
        self.dim
    }

    pub fn chroma_dim(&self) -> (u32, u32) {
        self.chroma_dim
    }

    pub fn fps(&self) -> u8 {
        self.fps
    }

    pub fn is_progressive(&self, img: usize) -> bool {
        self.is_progressive[img]
    }

    pub fn is_top_first_frame(&self, img: usize) -> bool {
        self.is_top_first_frame[img]
    }
}

// /!\ FIXME: if there is no number, return 0.
fn get_number<T: Iterator<Item = char>>(iter: &mut Peekable<T>) -> u32 {

    let mut number: u32 = 0;

    // While we can convert the next char into a digit, we add it to the
    // number and consume the char.
    while let Some(c) = iter.peek() {

        match c.to_digit(10) {
            Some(d) => { number = number*10 + d; iter.next(); },
            None => { break; }
        }
    }
    number
}

// /!\ FIXME: only work if all the words have differents first letter.
fn is_word<T: Iterator<Item = char>>(iter: &mut Peekable<T>, word_ref: &str)
                                                                    -> bool {

    // For all the letters of the reference word ...
    for cref in word_ref.chars() {

        // ... check if the next letter is correct.
        match iter.peek() {
            Some(c) => {
                if *c == cref {
                    iter.next();
                } else {
                    return false;
                }
            }
            None => { return false; }
        }
    }

    true
}
