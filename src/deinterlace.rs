use image::{GrayImage, RgbImage};
use crate::convert;
use crate::video_flags::VideoFlags;


pub struct DeinterlacedFrames {

    frames: [RgbImage; 2],
    is_current_is_first: bool,
    empty: bool
}


impl DeinterlacedFrames {

    // If the original image is progressive, returns in RGB format. If not
    // deinterlaced it. If displacement_threshold is less than 0, BOB will be
    // used. For any other positive value, spatial adaptive deinterlacer will
    // be used.
    pub fn new(orignal: &GrayImage,
                idx: usize,
                flags: &VideoFlags,
                displacement_threshold: i32) -> Self {

        if flags.is_progressive(idx) {
            DeinterlacedFrames {
                frames: [RgbImage::new(0,0),
                        convert::yuv2rgb(orignal,
                                            flags.dimensions(),
                                            flags.chroma_dim())],
                is_current_is_first: false,
                empty: false
            }
        } else {
            DeinterlacedFrames {
                frames: Self::deinterlace(orignal,
                                            idx,
                                            flags,
                                            displacement_threshold),
                is_current_is_first: true,
                empty: false
            }
        }
    }

    pub fn empty(&self) -> bool {

        self.empty
    }

    pub fn next(&mut self) -> &RgbImage {

        if self.is_current_is_first {
            self.is_current_is_first = false;
            &self.frames[0]
        } else {
            self.empty = true;
            &self.frames[1]
        }
    }

    // Deinterlaced the frames in orignal using BOB is displacement_threshold is
    // less than 0. If not, a spatial adaptive deinterlacer is used. The image
    // is cut in small piece and we check if they had moved since last frame. If
    // not we can use WEAVER (and thus having a better resolution !) instead of
    // BOB.
    // idx is the number of the image, this is used to read the correct flags.
    fn deinterlace(orignal: &GrayImage,
                    idx: usize,
                    flags: &VideoFlags,
                    displacement_threshold: i32) -> [RgbImage; 2] {

        // Convert the image to RBG and upscale the chroma.
        // TODO: do it on the fly !
        let orignal = convert::yuv2rgb(orignal,
                                        flags.dimensions(),
                                        flags.chroma_dim());
        let (w, h) = orignal.dimensions();
        let mut even_frame = RgbImage::new(w, h);
        let mut odd_frame = RgbImage::new(w, h);

        // Cut the image in 20x20 blocks.
        let (wsize_x, wsize_y) = (20, 20);
        let n_block_x = (w as f64 / wsize_x as f64).ceil() as usize;
        let n_block_y = (h as f64 / wsize_y as f64).ceil() as usize;

        let mut is_moving = vec!(false; n_block_x*n_block_y);

        // Search if the block is less than 5 pixel away.

        if displacement_threshold != 0 {

            let search_range = 5;

            // For each window (denoted by the lower left pixel)...
            for j in (0..h).step_by(wsize_y) {
                for i in (0..w).step_by(wsize_x) {

                    let mut min_diff = i32::MAX;

                    // ... and every offset ...
                    for offset_x in 0..search_range {
                        for offset_y in 0..search_range {

                            let mut mad = 0;

                            // ... evaluate the MAD (on the luminance) between
                            // the original window and the shifted on.
                            for wi in 0..wsize_y {
                                for wj in 0..wsize_x {

                                    let wi = wi as u32;
                                    let wj = wj as u32;

                                    let idx_off_x = i+wi+offset_x;
                                    let idx_off_y = (j|1)+wj+offset_y;

                                    if idx_off_x >= w || idx_off_y >= h {
                                        continue;
                                    }

                                    let original_pix = *orignal.get_pixel(
                                                            i+wi, (j&!1)+wj);
                                    let shifted_pix = *orignal.get_pixel(
                                                            idx_off_x, idx_off_y);

                                    let gray1 = (original_pix[0]/3
                                                + original_pix[1]/3
                                                + original_pix[2]/3) as i32;

                                    let gray2 = (shifted_pix[0]/3
                                                + shifted_pix[1]/3
                                                + shifted_pix[2]/3) as i32;

                                    mad += (gray1 - gray2).abs();
                                }
                            }

                            if mad < min_diff {
                                min_diff = mad;
                            }
                        }
                    }

                    // We have not found the block, it has moved to far.
                    if min_diff > displacement_threshold {

                        let i = i as usize;
                        let j = j as usize;
                        let idx = ((j/wsize_y)*n_block_x + i/wsize_y) as usize;
                        is_moving[idx] = true;
                    }
                }
            }
        }

        // Finally, we can deinterlace.
        for j in 0..h {
            for i in 0..w {

                let idx = ((j as usize) / wsize_y)*n_block_x
                            + (i as usize)/wsize_x;
                if is_moving[idx] {

                    // If the window has mouvement in it, deinterlace in BOB.
                    even_frame.put_pixel(i, j, *orignal.get_pixel(i, j & !1));
                    odd_frame.put_pixel(i, j, *orignal.get_pixel(i, j | 1));
                } else {
                    // If not use WEAVER.
                    even_frame.put_pixel(i, j, *orignal.get_pixel(i, j));
                    odd_frame.put_pixel(i, j, *orignal.get_pixel(i, j));
                }
            }
        }

        // Return the frame in the correct order.
        if flags.is_top_first_frame(idx) {
            [odd_frame, even_frame]
        } else {
            [even_frame, odd_frame]
        }
    }
}
